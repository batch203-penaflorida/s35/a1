const express = require("express");
const { request } = require("http");
// Express
const mongoose = require("mongoose");
// Mongoose is a package that allows us to create Schemas to model our data structures and to manipulate our database using different access methods.

const app = express();
const port = 3001;
// [SECTION] MongoDB Connection
// Syntax:
/* 
      mongoose.connect("<MongoDB Atlas Connection String>", 
      {
         // Allow us to avoid any current and future errors while connecting to mongoDB
         useNewUrlParser: true,
         useUnifiedTopology:true
      });
*/

mongoose.connect(
  "mongodb+srv://admin:admin@zuitt-bootcamp.m9kt1ev.mongodb.net/batch203_to-do?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"));

db.once("open", () => console.log("We're connected to the cloud database."));

// [SECTION] Moongose Schemas the structure of the documents to be written in the database
// Schemas acts as blueprints to our data.
// Syntax :
/* 
   const schemaName = new mongoose.Schema({<keyvalue: pair>})
*/
// name & status
// "required" is used to specifiy that a field must not be empty.
// ""default" is used if a field value is not supplied.
const taskSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Task name is required."],
  },
  status: {
    type: String,
    default: "pending",
  },
});

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, "Username is required."],
  },
  password: {
    type: String,
    required: [true, "Password is required."],
  },
});

// [SECTION] Models

// Uses schema and use it to create/instantiate documents/object that follows our schema structure.
// Acts as a middleman to our mongoose and backend.

// The variable/object that will be create can be used to run commands for interacting with our database.

// Syntax: const VariableName = mongoose.model("collectionName", schemaName);

// The first parameter of the Mongoose model method indicates the collection in where to store the data

// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection

// Task & "Task" is both capitalized following the MVC approach for naming conventions

// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form

const User = mongoose.model("User", userSchema);

const Task = mongoose.model("Task", taskSchema);

app.use(express.json()); // Allows app to read json data

app.use(express.urlencoded({ extended: true }));

// Business Logic
/*
    1. Add a functionality to check if there are duplicate tasks
        - If the task already exists in the database, we return an error message "Duplicate task found"
        - If the task doesn't exist in the database, we add it in the database
        A task exists if:
            - result from the query is not null
            - result.name is equal to req.body.name
    2. The task data will be coming from the request's body
    3. Create a new Task object with a "name" field/property
    4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
// Getting all the tasks
app.post("/signup", (req, res) => {
  User.findOne({ username: req.body.username }, (err, result) => {
    if (result !== null && result.username === req.body.username) {
      return res.send("Duplicate user found!");
    } else {
      let newUser = new User({
        username: req.body.username,
        password: req.body.password,
      });
      newUser.save((saveErr, savedTask) => {
        if (saveErr) {
          return console.error(saveErr);
        } else {
          return res.status(201).send("New User created!");
        }
      });
    }
  });
});

app.post("/tasks", (req, res) => {
  // "findOne" is a Mongoose method that acts similar to "find" of MongoDB
  // findOne() returns the first document that matches the search criteria
  // If there are no matches, the value of result is null
  // "err" is a shorthand naming convention for errors.
  // ModelName.findOne({<criteria>}, callbackfunction(err, result)=>{
  //})
  Task.findOne({ name: req.body.name }, (err, result) => {
    if (result !== null && result.name === req.body.name) {
      return res.send("Duplicate task found!");
    } else {
      // "newTask" was created/instantiaited from the Mongoose schema and will gain access to ".save" method
      let newTask = new Task({
        name: req.body.name,
      });

      newTask.save((saveErr, savedTask) => {
        if (saveErr) {
          return console.error(saveErr);
        } else {
          return res.status(201).send("New Task created!");
        }
      });
    }
  });
});

// Business Logic
/*
        1. Retrieve all the documents
        2. If an error is encountered, print the error
        3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks", (req, res) => {
  Task.find({}, (err, result) => {
    if (err) {
      return console.log(err);
    } else {
      return res.status(200).send({ Tasks: result });
    }
  });
});

// Allows your app to read data from forms.
/* 
 
*/

app.listen(port, () => console.log(`Server running at port ${port}`));
